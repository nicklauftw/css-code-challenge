window.onload = () => {
  console.log("Not much JS, just CSS in this project");
  var menuOpen = document.querySelector('[data-menu-open]');
  var menuClose = document.querySelector('[data-menu-close]')
  var menuElement = document.getElementsByClassName('menu');
  var menuToggle = function(el) {
    el[0].classList.toggle('menu--open');
    return;
  }
  menuOpen.addEventListener('click', function() {
    menuToggle(menuElement);
  });
  menuClose.addEventListener('click', function() {
    menuToggle(menuElement);
  });
}
