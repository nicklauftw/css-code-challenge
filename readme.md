I used `node-sass` through a NPM task to watch and compile `.scss` files. The CSS output is compiled and checked into the repo. How I developed this project are the following commands.

`npm install`

`npm run dev`

`http-server` (simple static node server installed globally)

Due to the relative path used for images, the project has to have a server running to serve the images.
